from rest_framework import serializers

class EmailSerializer(serializers.Serializer):
    to = serializers.EmailField()
    subject = serializers.CharField()
    message = serializers.CharField()
    document = serializers.FileField(required=False)
# from django.contrib.auth import get_user_model
# from celery import shared_task
# from django.core.mail import send_mail
# from django_celery_project import settings

# @shared_task(bind = True)
# def send_mail_func(self):
#     users = get_user_model().objects.all()
#     for user in users:
#         mail_subject = "Celery Testing"
#         message = "message body"
#         to_email = user.email
#         send_mail(
#             subject=mail_subject,
#             message=message,
#             from_email=settings.EMAIL_HOST_USER,
#             recipient_list=[to_email],
#             fail_silently=True,
#         )
#     return "Donef"
# tasks.py

from celery import shared_task
from django.core.mail import send_mail
from django_celery_project import settings
from django.core.mail import EmailMessage


@shared_task
def send_email_task(subject, message, recipient ):
    email = EmailMessage(subject, message, to=[recipient])
    # if document:
    #     email.attach(document.name, document.read(), document.content_type)
    email.send()


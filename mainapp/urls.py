# from django.urls import path, include
# from . import views


# urlpatterns = [
#     path("", views.test, name="test")
# ]
# urls.py

from django.urls import path
from .views import EmailView

urlpatterns = [
    path('send-email/', EmailView.as_view(), name='send_email'),
]

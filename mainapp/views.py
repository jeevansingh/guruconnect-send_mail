# from django.shortcuts import render
# from django.http import HttpResponse
# from .tasks import send_mail_func
# #from send_mail_app.tasks import send_mail_func

# # Create your views here.
# def test(request):
#     send_mail_func.delay()
#     return HttpResponse("Donel")

# views.py

# views.py

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from .serializers import EmailSerializer
from .tasks import send_email_task
from django.shortcuts import render

class EmailView(APIView):
    def get(self, request):
        return render(request, 'email_form.html')
    
# views.py

from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import EmailSerializer
from .tasks import send_email_task

class EmailView(APIView):
    # def get(self, request):
    #     return render(request, 'email_form.html')

    def post(self, request):
        serializer = EmailSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.validated_data
            send_email_task.delay(data['subject'], data['message'], data['to'])
            return Response({'message': 'Email sending task has been scheduled.'})
        else:
            return Response(serializer.errors, status=400)
